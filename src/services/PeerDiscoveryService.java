package services;

import application.Application;
import common.Node;
import configs.NetworkConfig;
import helpers.Constants;
import protocols.PeerDiscoveryProtocol;

public class PeerDiscoveryService extends Thread {
	/*
	 * This thread is ran periodically. It uses the discovery protocols to discover
	 * the network
	 */
	public void run() {
		while (true) {
			try {
				Thread.sleep(NetworkConfig.discovery_time);
			} catch (InterruptedException e) {
				System.out.println(Constants.ERROR + " Discovery protocol thread unexpected interupt: " + e.getMessage());
				e.printStackTrace();
			}
			System.out.println(Constants.DISCOVERY + "Peer discovery protocol update");
			// go through nodes that should be asked for new peers
			Application.readWriteLock.readLock().lock();
			for (Node n : Application.known_nodes.values()) {
				if (n.isConnected()) { // check if the node needs to be asked
					// update timers
					n.setLast_update(System.currentTimeMillis());
					// should dynamically increase the timeout also
					n.setTimeout(n.getTimeout() + 1000); // testing with 1s
					// send update request
					PeerDiscoveryProtocol.requestAddr(n);
				}
			}
			Application.readWriteLock.readLock().unlock();

		}
	}
}
