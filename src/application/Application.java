package application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import common.Node;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.NetworkUtil;
import services.PeerDiscoveryService;

public class Application {

	public static HashMap<String, Node> known_nodes = new HashMap();
	public static ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	public static ServerSocket serverSocket;
	public static ExecutorService executor = Executors.newCachedThreadPool();

	public static void main(String[] args) {

		// discover my own IP, works for LAN in development mode.
		NetworkConfig.local_ip = NetworkUtil.discoverExternalIP();
		// generate a unique id using network interfaces as a seed
		NetworkConfig.node_id = NetworkUtil.createNodeId();
		System.out.println(Constants.SERVER + " Created node id " + NetworkConfig.node_id);
		if (!NetworkConfig.trusted_node_ip.equals(NetworkConfig.local_ip)) {
			Node trustedNode = new Node(NetworkConfig.trusted_node_ip, NetworkConfig.trusted_node_port);
			trustedNode.connect();
			executor.execute(trustedNode);
			known_nodes.put(trustedNode.getId(), trustedNode);
		}
		executor.execute(new PeerDiscoveryService());

		while (true) {
			try {
				if (serverSocket == null) {
					serverSocket = new ServerSocket(NetworkConfig.server_port);
					System.out.println(Constants.SERVER + "Started listening on port " + NetworkConfig.server_port);
				}
			} catch (IOException e) {
				System.out.println(Constants.SERVER + "Failed to aquire socket: " + NetworkConfig.server_port);
				e.printStackTrace();
			}
			try {
				Socket client = serverSocket.accept();
				Node n = new Node(client.getInetAddress().getHostAddress(), client.getPort(), client);
				executor.execute(n);
				System.out.println(Constants.SERVER + "Accepted socket connection from " + client.getInetAddress() + ":"
						+ client.getPort());
				System.out.println(Constants.SERVER + "Established connection with new node " + n.getId());
			} catch (IOException e) {
				System.out.println(Constants.SERVER + "Client failed to connect");
				e.printStackTrace();
			}
		}
	}
}
