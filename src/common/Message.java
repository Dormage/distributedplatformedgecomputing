package common;
import helpers.ProtocolHelper;

public class Message {

	public String header; //type of message
	public String content; //json encoded payload
	public String type;
		
	public Message(String header, String content, String type) {
		this.header = header;
		this.content = content;
		this.type = type;
	}
	
}
