package common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import application.Application;
import helpers.Constants;
import helpers.ProtocolHelper;
import protocols.HandshakeProtocol;
import protocols.PeerDiscoveryProtocol;

public class Node implements Runnable {
	String id;
	private String address;
	private int port;
	Socket connection;
	BufferedReader in;
	BufferedWriter out;
	private long last_update;
	private long timeout = 5000;
	long timestamp;
	boolean handshake=false;


	public boolean isHandshake() {
		return handshake;
	}

	public void setHandshake(boolean handshake) {
		this.handshake = handshake;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public long getLast_update() {
		return last_update;
	}

	public void setLast_update(long last_update) {
		this.last_update = last_update;
	}

	public Node(String address, String port) {
		this.address = address;
		this.port = Integer.parseInt(port);
	}

	public Node(String address, int port) {
		this.address = address;
		this.port = port;
	}
	
	public Node(String address, int port, String id) {
		this.address = address;
		this.port = port;
		this.id = id;
	}


	public Node(String address, int port, Socket connection) throws IOException {
		this.address = address;
		this.port = port;
		this.connection = connection;
		in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
		this.last_update = System.currentTimeMillis();
		HandshakeProtocol.sendHandshake(this);
	}

	public boolean connect() {
		if (port != 0 && address != null) {
			try {
				connection = new Socket(address, port);
				in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
				HandshakeProtocol.sendHandshake(this);
				this.last_update = System.currentTimeMillis();
				return true;
			} catch (UnknownHostException e) {
				return false;
			} catch (IOException e) {
				return false;
			}
		} else {
			return false;
		}
		
	}

	@Override
	public void run() {
		System.out.println(Constants.MISC + " listener thread started...");
		String received;
		try {
			while (connection.isConnected() && (received = in.readLine()) != null) {
				System.out.println(Constants.COMM + "Node " + id + " sent: " + received);
				Message message = ProtocolHelper.gson.fromJson(received, Message.class);
				if (message != null) {
					switch (message.header) {
					case "handshake":
						HandshakeProtocol.digest(message, this);
						if(this.id !=null && !Application.known_nodes.containsKey(id)) {
							Application.readWriteLock.writeLock().lock();
							Application.known_nodes.put(id, this);
							System.out.println(Constants.MISC + " Node " +this.id + " added to known nodes.");
							Application.readWriteLock.writeLock().unlock();
						}
						break;
					case "addr":
						PeerDiscoveryProtocol.digest(message, this);
						break;
					case "ping":
						//pong
						break;
					case "message":
						break;
					default:
						break;
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Error reading from socket...");
		}
	}

	public String read() {
		try {
			if (in.ready()) {
				return in.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void send(String data) {
		try {
			out.write(data);
			out.newLine();
			flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void flush() {
		try {
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean close() {

		boolean result = true;
		try {
			in.close();
			out.close();
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public boolean isConnected() {
		if (connection == null) {
			return false;
		} else {
			return connection.isConnected();
		}
	}

	/* Getters and setters */
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String toString() {
		return address + ":" + port;
	}

	public void sendMessage(Message message) {
		send(ProtocolHelper.gson.toJson(message));
	}
	public void increaseTimeout() {
		this.timeout = this.timeout *2;
	}
}