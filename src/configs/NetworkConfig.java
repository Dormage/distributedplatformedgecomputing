package configs;

public class NetworkConfig {
	public static int discovery_time = 5000; // initial delay used by peer discovery protocol
	public static int requiered_nodes = 10; // number of required nodes
	public static int server_port = 5000;
	public static long default_discovery_timeout = 2000L;
	public static String trusted_node_ip = "172.17.0.2";
	public static int trusted_node_port = 5000;
	public static boolean isProduction = false;
	public static String local_ip = "";
	public static int local_port = 0;
	public static String node_id; 
}
