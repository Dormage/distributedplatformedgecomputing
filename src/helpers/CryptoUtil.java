package helpers;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoUtil {

	public static String Hash(String message) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(message.getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();

		String hex = String.format("%064x", new BigInteger(1, digest));
		return hex;
	}

	public static boolean Verify(String message, String hash) {
		String received = null;
		try {
			received = Hash(message);
		} catch (NoSuchAlgorithmException e) {
			System.out.println(Constants.ERROR + " Hashing error! " + e.getMessage());
		}
		if (received.equals(hash)) {
			return true;
		} else {
			return false;
		}
	}
}
