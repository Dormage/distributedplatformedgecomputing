package helpers;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.HashMap;

import configs.NetworkConfig;

public class NetworkUtil {
	private static final int NODE_ID_BITS = 10;
	private static final int maxNodeId = (int) (Math.pow(2, NODE_ID_BITS) - 1);

	/*
	 * Various provides are used to try and discover the external IP addres of the
	 * node. The address is then broadcasted to all new nodes in the peer discovery
	 * protocol. This should only be ran in production evniorment
	 */
	public static String discoverExternalIP() {
		if (NetworkConfig.isProduction) {
			// request @ 91.198.22.70 on port 80
			// request @ 74.208.43.192 on port 80
			// request @ www.showmyip.com
			return null;
		} else {
			try {
				return InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				System.out.println(Constants.ERROR + " Unable to get local IP address");
			}
		}
		return null;
	}

	public static String hash(String password) {
		try {
			MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			String salt = "some_random_salt";
			String passWithSalt = password + salt;
			byte[] passBytes = passWithSalt.getBytes();
			byte[] passHash = sha256.digest(passBytes);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < passHash.length; i++) {
				sb.append(Integer.toString((passHash[i] & 0xff) + 0x100, 16).substring(1));
			}
			String generatedPassword = sb.toString();
			return generatedPassword;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String createNodeId() {
		int nodeId;
		try {
			StringBuilder sb = new StringBuilder();
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = networkInterfaces.nextElement();
				byte[] mac = networkInterface.getHardwareAddress();
				if (mac != null) {
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X", mac[i]));
					}
				}
			}
			nodeId = sb.toString().hashCode();
		} catch (Exception ex) {
			nodeId = (new SecureRandom().nextInt());
		}
		nodeId = nodeId & maxNodeId;
		return hash(""+nodeId);
	}
}
