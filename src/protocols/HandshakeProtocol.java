package protocols;

import common.Message;
import common.Node;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.ProtocolHelper;

public class HandshakeProtocol {
	
	public static void sendHandshake(Node node) {
		String payload[] = new String[4];
		payload[0] = NetworkConfig.node_id;
		payload[1] = ""+System.currentTimeMillis();
		payload[2] = NetworkConfig.local_ip;
		payload[3] = ""+NetworkConfig.server_port;
		Message m = new Message("handshake", ProtocolHelper.gson.toJson(payload),"request");
		node.send(ProtocolHelper.gson.toJson(m));
		System.out.println(Constants.PROTOCOL +  " Sending handshake to new connection ");
	}

	public static void digest(Message message, Node node) {
		String[] payload = ProtocolHelper.gson.fromJson(message.content, String[].class);
		node.setId(payload[0]);
		node.setTimestamp(Long.parseLong(payload[1]));
		node.setAddress(payload[2]);
		node.setPort(Integer.parseInt(payload[3]));
		node.setHandshake(true);
		System.out.println(Constants.PROTOCOL + " Received handshake from node " + node.getId());
	}
}
