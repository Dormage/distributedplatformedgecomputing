# Decentralized architecture for Edge computing



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Eclipse
* Docker


### Installing


*  Clone the entire repository
*  Use the Eclipse to import an existing project to workspace and point it at the root of the repository
*  Add external jars (libraries) located in the lib folder to the build path


### Building

*  Export the project as a runnable jar to the docker folder (i.e. Node.jar)
*  Cd to the docker folder and build a docker image: `docker build --tag node .`

## Running the tests

Start as many terminal windows and run `docker run node`


## Dependancies

* [GSON](https://github.com/google/gson) - Google's json library for Java
* [Docker](https://www.docker.com/) - Container framework

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Aleksandar Tošič** - *Initial work* - [Dormage](https://gitlab.com/Dormage)
* **Michael Mrissa** 
* **Jernej Vičič** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments



